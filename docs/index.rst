.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the S3CURITY.EU blog
====================================================

.. include:: ../README.rst

.. toctree::
   :caption: General
   :maxdepth: 2

   introduction


.. toctree::
    :maxdepth: 2
    :numbered:
    :caption: Penetration Testing

    pentest/introduction

.. toctree::
    :maxdepth: 3
    :numbered:
    :caption: Red Teaming

    redteam/introduction
